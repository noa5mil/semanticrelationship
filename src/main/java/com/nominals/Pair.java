package com.nominals;


public class Pair<R,L> {
    private final L left;
    private final R right;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public Pair(Pair p){
        this.right = (R) p.right;
        this.left = (L) p.left;
    }

    public L getLeft() { return left; }
    public R getRight() { return right; }

    @Override
    public String toString() {
        return this.left + " " + this.right;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.left.equals(((Pair<R,L>)obj).left) && this.right.equals(((Pair<R,L>)obj).right));
    }

    @Override
    public int hashCode() {
        return left.hashCode() * right.hashCode();
    }
}
