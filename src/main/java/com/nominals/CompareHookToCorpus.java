package com.nominals;

import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;


public class CompareHookToCorpus {
    private static HashMap<String, String> wordsMap = new HashMap<>();
    private static HashMap<String, String> hooksMap = new HashMap<>();

    public final static Regions REGION = Regions.US_EAST_1;

    private static String bucketName = "hagarnoadsp";
    private static String wordCountOutput = "nominals/wordCount1-output/part-r-";
    private static String hookfile = "nominals/hook/hook";
    private static AmazonS3 S3 = AmazonS3ClientBuilder.standard()
            .withRegion(REGION)
            .build();

    //download a file from S3
    private static void downloadFilesFromS3(String filepath, HashMap<String, String> saveMap) {
        S3Object file = null;
        System.out.println("Listing objects");
        final ListObjectsV2Request req = new ListObjectsV2Request()
                .withBucketName(bucketName)
                .withPrefix(filepath);
        ListObjectsV2Result result = S3.listObjectsV2(req);
        for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
            try {
                file = S3.getObject(new GetObjectRequest(bucketName, objectSummary.getKey()));
                insertToMap(file, saveMap);
            } catch (AmazonClientException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertToMap(S3Object file, HashMap<String, String> saveMap) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getObjectContent()));
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                line = line.toString().replace("\t", " ");
                String[] splitWordsBySpace = line.split(" ");
                if (!saveMap.containsKey(splitWordsBySpace[0])) {
                    saveMap.put(splitWordsBySpace[0],splitWordsBySpace[1]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        downloadFilesFromS3(wordCountOutput, wordsMap);
        downloadFilesFromS3(hookfile, hooksMap);
        int count = 0;
        for (String hook:hooksMap.keySet()){
            if(wordsMap.containsKey(hook)){
                System.out.println("word: " + hook + " count : " + wordsMap.get(hook));
                count ++;
            }
            else{
                System.out.println("word: " + hook + "  doesn't exist in corpus");
            }
        }
        System.out.println("hooks in corpus: " + count);
    }


}
