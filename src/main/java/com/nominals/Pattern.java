package com.nominals;

import java.util.HashSet;

public class Pattern {
    private boolean isCore;
    private String patt;
    private HashSet<Pair<String, String>> pairList = new HashSet<>();

    public Pattern(Pair<String,String> hookTarget, String p){
        this.isCore = false;
        patt = p;
        pairList.add(hookTarget);
    }
    public Pattern(Pattern p){
        this.pairList = new HashSet<>(p.pairList);
        this.patt = p.patt;
        this.isCore = p.isCore;
    }
    public void setCore(boolean core) {
        isCore = core;
    }

    public void setPairList(HashSet<Pair<String, String>> pairList) {
        this.pairList = pairList;
    }

    public void addPairToPattern(Pair<String,String> toAdd){
        pairList.add(toAdd);
    }

    public void addPairsToPattern(HashSet<Pair<String, String>> toAdd){
        pairList.addAll(toAdd);
    }

    public void setPatt(String patt) {
        this.patt = patt;
    }

    public HashSet<Pair<String, String>> getPairList() {
        return pairList;
    }

    public String getPatt() {
        return patt;
    }

    public boolean isCore() {
        return isCore;
    }

    @Override
    public boolean equals(Object other) {
        return this.patt.equals(((Pattern)other).patt);
    }

}
